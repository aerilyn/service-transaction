package main

import (
	"context"

	"gitlab.com/aerilyn/service-transaction/internal/app"
)

const InterruptionSignalKey = "interruptionSignal"

func main() {
	app, _, err := app.InjectApp()
	if err != nil {
		panic(err.Error())
	}
	err = app.SetRoute(context.Background())
	if err != nil {
		panic(err.Error())
	}

	err = app.AddPubsubRunner()
	if err != nil {
		panic(err.Error())
	}

	app.ApplicationDelegate.RunMessagePubsub(context.Background())
	app.RunServer()
}
