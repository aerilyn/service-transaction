GOCMD=go

.PHONY: runLocal
runLocal:
	@echo "===== RUNNING APLICATION ====="
	@go run cmd/app/main.go --env=.env

.PHONY: wire
wire:
	cd internal/app && wire || exit 1

.PHONY: serviceUp
serviceUp:
	@echo "===== RUNNING SERVICES ====="
	cd deployments && docker-compose up -d --build || exit 1

.PHONY: serviceDown
serviceDown:
	@echo "===== DOWN SERVICES ====="
	cd deployments && docker-compose down || exit 1

.PHONY: systemUp
systemUp:
	@echo "===== RUNNING SERVICES ====="
	cd deployments && docker-compose -f docker-compose.system.yml up -d --build || exit 1

.PHONY: systemDown
systemDown:
	@echo "===== RUNNING SERVICES ====="
	cd deployments && docker-compose -f docker-compose.system.yml down || exit 1

.PHONY: runTesting
runTesting:
	cd internal/app/example/usecase/usecaseimpl && go test ./... -cover -v || exit 1
