package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-transaction/internal/app/transaction/usecase"
)

type InsertOptions struct {
	Config config.ConfigEnv
}

type Insert struct {
	options InsertOptions
}

func NewInsert(options InsertOptions) *Insert {
	return &Insert{
		options: options,
	}
}

func (s *Insert) Insert(ctx context.Context, cmd usecase.InsertCommand) (*usecase.InsertDTO, errors.CodedError) {
	dto := usecase.InsertDTO{
		Status: true,
	}
	return &dto, nil
}
