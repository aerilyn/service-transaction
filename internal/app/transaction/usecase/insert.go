package usecase

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type InsertCommand struct {
	Status bool `json:"status"`
}

type InsertDTO struct {
	Status bool `json:"status"`
}

type Insert interface {
	Insert(ctx context.Context, cmd InsertCommand) (*InsertDTO, errors.CodedError)
}
