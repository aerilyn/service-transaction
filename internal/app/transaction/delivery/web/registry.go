package web

import (
	"gitlab.com/aerilyn/service-transaction/internal/app/transaction/usecase"

	"github.com/go-chi/chi"
)

type RegistryOptions struct {
	InsertTransaction usecase.Insert
}

type HandlerRegistry struct {
	options RegistryOptions
}

func NewExampleHandlerRegistry(options RegistryOptions) *HandlerRegistry {
	return &HandlerRegistry{
		options: options,
	}
}

func (h *HandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/example/test", func(r chi.Router) {
		r.Get("/", InsertWebHandler(h.options.InsertTransaction))
	})
	return nil
}
