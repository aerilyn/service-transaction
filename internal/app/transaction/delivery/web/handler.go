package web

import (
	"net/http"

	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/response"
	"gitlab.com/aerilyn/service-transaction/internal/app/transaction/usecase"
)

func InsertWebHandler(example usecase.Insert) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.InsertCommand{}
		result, err := example.Insert(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}
