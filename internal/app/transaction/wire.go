package transaction

import (
	"gitlab.com/aerilyn/service-transaction/internal/app/transaction/delivery/web"
	"gitlab.com/aerilyn/service-transaction/internal/app/transaction/usecase"
	"gitlab.com/aerilyn/service-transaction/internal/app/transaction/usecase/usecaseimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.RegistryOptions), "*"),
	web.NewExampleHandlerRegistry,
)

var validatorModuleSet = wire.NewSet()

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.InsertOptions), "*"),
	usecaseimpl.NewInsert,
	wire.Bind(new(usecase.Insert), new(*usecaseimpl.Insert)),
)
