package repository

import (
	"gopkg.in/mgo.v2/bson"
)

type FindOptions struct {
	Limit  int64
	Offset int64
	Filter bson.M
}

type ExampleRepository interface {
}
