package ent

import "time"

//transaction ent
type Transaction struct {
	ID                string         `json:"id" bson:"_id,omitempty"`
	TransactionID     string         `json:"transaction_id" bson:"transaction_id"`
	PaymentID         string         `json:"payment_id" bson:"payment_id"`
	UserID            string         `json:"user_id" bson:"user_id"`
	Discount          float32        `json:"discount" bson:"discount"`
	Total             float32        `json:"total" bson:"total"`
	TaxAmount         float32        `json:"tax_amount" bson:"tax_amount"`
	TotalMustPay      float32        `json:"total_must_pay" bson:"total_must_pay"`
	Status            string         `json:"status" bson:"status"`
	DiscountDetail    DiscountDetail `json:"discount_detail" bson:"discount_detail"`
	TransactionDetail []Item         `json:"transaction_detail" bson:"transaction_detail"`
	CreatedAt         time.Time      `json:"created_at" bson:"created_at"`
	UpdatedAt         time.Time      `json:"-" bson:"updated_at"`
}

type Item struct {
	ItemID   string  `json:"item_id" bson:"item_id"`
	Price    float32 `json:"price" bson:"price"`
	Quantity int     `json:"quantity" bson:"quantity"`
	Status   string  `json:"status" bson:"status"`
}

type DiscountDetail struct {
	DiscountCode  string  `json:"discount_code" bson:"discount_code"`
	DiscountAmout float32 `json:"discount_amount" bson:"discount_amount"`
}
