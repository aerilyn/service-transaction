package ent

import "time"

//discount ent
type Discount struct {
	ID           string    `json:"id" bson:"_id,omitempty"`
	DiscountCode string    `json:"discount_code"`
	Description  string    `json:"description"`
	Status       bool      `json:"status"`
	Credit       int       `json:"credit" bson:"credit"`
	ExpiredAt    time.Time `json:"expired_at" bson:"expired_at"`
	CreatedBy    string    `json:"-" bson:"created_by"`
	CreatedAt    time.Time `json:"-" bson:"created_at"`
	UpdatedAt    time.Time `json:"-" bson:"updated_at"`
}
