package app

import (
	discountweb "gitlab.com/aerilyn/service-transaction/internal/app/discount/delivery/web"
	healthcheckweb "gitlab.com/aerilyn/service-transaction/internal/app/healthcheck/delivery/web"
	transactionweb "gitlab.com/aerilyn/service-transaction/internal/app/transaction/delivery/web"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	HealthCheckHTTPHandlerRegistry *healthcheckweb.HealthCheckHandlerRegistry
	DiscountHTTPHandlerRegistry    *discountweb.HandlerRegistry
	TransactionHTTPHandlerRegistry *transactionweb.HandlerRegistry
}
