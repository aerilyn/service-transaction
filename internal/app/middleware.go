package app

import (
	"gitlab.com/aerilyn/service-library/middleware/header"
	"gitlab.com/aerilyn/service-library/middleware/log"
)

type RequiredMiddlewares struct {
	Header  *header.HeaderMiddlewareRegistry
	Logging *log.LoggingMiddlewareRegistry
	// NewRelic      *newrelic.NewReliMiddlewareRegistry
	// Auth          *auth.AuthMiddlewareRegistry
	// Authorization *authorization.AuthorizationMiddlewareRegistry
	// Watermill     *watermill.WatermillStdMiddlewareRegistry
}
