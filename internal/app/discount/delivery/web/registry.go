package web

import (
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/usecase"

	"github.com/go-chi/chi"
)

type RegistryOptions struct {
	ListDiscount   usecase.List
	InsertDiscount usecase.Insert
}

type HandlerRegistry struct {
	options RegistryOptions
}

func NewHandlerRegistry(options RegistryOptions) *HandlerRegistry {
	return &HandlerRegistry{
		options: options,
	}
}

func (h *HandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/admin", func(r chi.Router) {
		r.Route("/discount", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Post("/", InsertWebHandler(h.options.InsertDiscount))
			})
		})
	})

	r.Route("/discount/", func(r chi.Router) {
		r.Get("/", ListWebHandler(h.options.ListDiscount))
	})
	return nil
}
