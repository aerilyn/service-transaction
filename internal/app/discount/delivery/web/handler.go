package web

import (
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/middleware"
	"gitlab.com/aerilyn/service-library/response"
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/usecase"
)

func ListWebHandler(uc usecase.List) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		result, err := uc.List(ctx)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}

func InsertWebHandler(uc usecase.Insert) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		cmd := usecase.InsertCommand{}
		if decodeErr := render.DecodeJSON(r.Body, &cmd); decodeErr != nil {
			err := errors.NewInternalSystemError().CopyWith(errors.Message(decodeErr.Error()))
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		cmd.CreatedBy = ctx.Value(middleware.UserIDKey).(string)
		result, err := uc.Insert(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusCreated, nil})
	}
}
