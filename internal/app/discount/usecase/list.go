package usecase

import (
	"context"
	"time"

	"gitlab.com/aerilyn/service-library/errors"
)

type ListDTO struct {
	Discounts []Discount `json:"discounts"`
}

type Discount struct {
	DiscountID   string    `json:"discount_id"`
	DiscountCode string    `json:"discount_code"`
	Description  string    `json:"description"`
	Credit       int       `json:"credit" bson:"credit"`
	ExpiredAt    time.Time `json:"expired_at" bson:"expired_at"`
}

type List interface {
	List(ctx context.Context) (*ListDTO, errors.CodedError)
}
