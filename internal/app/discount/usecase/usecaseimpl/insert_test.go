package usecaseimpl

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	configMock "gitlab.com/aerilyn/service-library/config/mock"
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/usecase"
)

func TestItemCreate(t *testing.T) {
	ctx := context.Background()
	cmd := usecase.InsertCommand{}
	dto := usecase.InsertDTO{
		Status: true,
	}
	Convey("Testing Example Test", t, func() {
		mockCtrl := gomock.NewController(t)
		Reset(func() {
			mockCtrl.Finish()
		})
		configMock := configMock.NewMockConfigEnv(mockCtrl)
		createOptions := InsertOptions{
			Config: configMock,
		}
		uc := NewInsert(createOptions)
		Convey("when all process not return error  should return error nil", func() {
			res, errCode := uc.Insert(ctx, cmd)
			So(res, ShouldNotBeNil)
			So(errCode, ShouldBeNil)
			So(res, ShouldResemble, &dto)
		})
	})
}
