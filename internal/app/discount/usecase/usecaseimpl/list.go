package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/usecase"
)

type ListOptions struct {
	Config config.ConfigEnv
}

type List struct {
	options ListOptions
}

func NewList(options ListOptions) *List {
	return &List{
		options: options,
	}
}

func (s *List) List(ctx context.Context) (*usecase.ListDTO, errors.CodedError) {
	dto := usecase.ListDTO{
		Discounts: []usecase.Discount{},
	}
	return &dto, nil
}
