package usecase

import (
	"context"
	"time"

	"gitlab.com/aerilyn/service-library/errors"
)

type InsertCommand struct {
	DiscountCode string    `json:"discount_code"`
	Description  string    `json:"description"`
	Status       bool      `json:"status"`
	Credit       int       `json:"credit" bson:"credit"`
	ExpiredAt    time.Time `json:"expired_at" bson:"expired_at"`
	CreatedBy    string    `json:"created_by" bson:"created_by"`
}

type InsertDTO struct {
	Status bool `json:"status"`
}

type Insert interface {
	Insert(ctx context.Context, cmd InsertCommand) (*InsertDTO, errors.CodedError)
}
