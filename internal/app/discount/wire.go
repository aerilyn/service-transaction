package discount

import (
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/delivery/web"
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/usecase"
	"gitlab.com/aerilyn/service-transaction/internal/app/discount/usecase/usecaseimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.RegistryOptions), "*"),
	web.NewHandlerRegistry,
)

var validatorModuleSet = wire.NewSet()

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.InsertOptions), "*"),
	usecaseimpl.NewInsert,
	wire.Bind(new(usecase.Insert), new(*usecaseimpl.Insert)),
	wire.Struct(new(usecaseimpl.ListOptions), "*"),
	usecaseimpl.NewList,
	wire.Bind(new(usecase.List), new(*usecaseimpl.List)),
)
