package mongo

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/aerilyn/service-library/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	db        *mongo.Database
	onceMongo sync.Once
)

func ProvideClient(cfg *config.Config) *mongo.Database {
	onceMongo.Do(func() {
		fmt.Println("connect mongodb with connection => ", cfg.Get(config.MONGODB_URL))
		client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(cfg.Get(config.MONGODB_URL)))
		if err != nil {
			panic(err.Error())
		}

		db = client.Database(cfg.Get(config.MONGODB_DATABASE))
	})
	return db
}
